from cgitb import handler
from flask import Flask, request, abort
from linebot import LineBotApi, WebhookHandler
from linebot.exceptions import InvalidSignatureError
from linebot.models import MessageEvent, TextMessage, TextSendMessage
import configparser
import crawer
import json
import requests
from random import randint

# linebot的基本資料
config = configparser.ConfigParser()
config.read('config.ini')
ts = []

# 爬蟲&設定推播樣式可參考 https://developers.line.biz/en/reference/messaging-api/#send-broadcast-message
results = crawer.pttCrawer("Stock")
for result in results:
    print(result["title"])
    ts.append({
        "thumbnailImageUrl": "https://source.unsplash.com/featured/?stock,money?"+str(randint(1, 100)),
        "imageBackgroundColor": "#FFFFFF",
        "title": result["title"],
        "text": result["url"],
        "defaultAction": {
            "type": "uri",
            "label": "查看詳細資料",
            "uri": result["url"]
        },
        "actions": [
            {
                "type": "uri",
                "label": "查看",
                "uri": result["url"]
            }
        ]
    })


# 因為line推定一次只能推10則訊息
if len(ts) > 10:
    ts = ts[0:10]

prejson = {
    "messages": [
        {
            "type": "template",
            "altText": "每日新聞",
            "template": {
                "type": "carousel",
                "columns": ts,
                "imageAspectRatio": "rectangle",
                "imageSize": "cover"
            }
        }
    ]
}
print(prejson)
payload = json.dumps(prejson)
print(payload)
headers = {
    'Authorization': 'Bearer '+config.get('line_bot', 'Channel_Access_Token'),
    'Content-Type': 'application/json'
}
url = "https://api.line.me/v2/bot/message/broadcast"
response = requests.request("POST", url, headers=headers, data=payload)
print(response.status_code)


